#pragma once

#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <fstream>

#undef max
#undef min

class Utility
{
public:

	template< typename T >
	struct pos_t
	{
		T x, y;
	};

	template< typename T >
	struct square
	{
		pos_t< T >	left_top;
		pos_t< T >	size;
	};

	template< typename T >
	static constexpr T clamp( T value, T minimum, T maximum )
	{
		return std::min( std::max( value, minimum ), maximum );
	}

	static std::vector< std::string > split( const std::string& input, char delimiter = ' ' )
	{
		std::istringstream stream( input );
		std::string field;
		std::vector< std::string > result;

		while( std::getline( stream, field, delimiter ) ) {
			result.push_back( field );
		}

		return result;
	}

	static bool is_exists( std::string const& f )
	{
		return !std::fstream( f ).fail();
	}

	static void make_new_file( std::string const& file_name )
	{
		std::ofstream ofs( file_name );
	}
};
