#pragma once

#include "Debug.h"
#include "SystemContext.h"
#include "GameContext.h"
#include "VirtualController.h"
#include "Player.h"
#include "KeyConfig.h"

class Game :
	public GameContext
{
public:

	enum class replay_mode
	{
		record,
		play,
	};

private:

	static constexpr Utility::pos_t< int >	GAMEAREA_SIZE     = { 384, 448 };
	static constexpr Utility::pos_t< int >	GAMEAREA_LEFT_TOP = { 128,  16 };

	SystemContext*	system;
	unsigned		game_count;
	VCtrlPtr		controller;
	Player			player;

public:

	Game( SystemContext* sys );
	~Game();

	bool proc();
	void draw() const;

	Utility::square< int > get_game_area() const override;
	unsigned get_game_count() const override;
	VCtrlPtr get_input() const override;
};

