#pragma once

#include <memory>
#include "Keyboard.h"
#include "Joypad.h"

class GettableInputDevice
{
public:
	GettableInputDevice() = default;
	virtual ~GettableInputDevice() = default;

	virtual std::shared_ptr< Keyboard > get_keyboard() const = 0;
	virtual std::shared_ptr< Joypad > get_joypad() const = 0;
};

