#include <DxLib.h>
#include "Game.h"
#include "GameContext.h"
#include "ReplayRecorder.h"
#include "ReplayPlayer.h"
#include "VirtualController.h"
#include "Player.h"
#include "KeyConfig.h"

Game::Game( SystemContext* sys ) :
	system( sys ),
	game_count( 0U ),
	controller(),
	player( this )
{
	if( false ) {
		controller = std::make_shared< KeyConfig >( sys, std::make_shared< ReplayRecorder >( this ) );
	} else {
		controller = std::make_shared< ReplayPlayer >( this, "replay.rpy" );
	}
}

Game::~Game()
{
}

bool Game::proc()
{
	++game_count;
	player.proc();
	return true;
}

void Game::draw() const
{
	// TODO: この部分を変更し、DxLib臭を消す	const auto area = game->get_game_area();
	DrawBox(
		GAMEAREA_LEFT_TOP.x, GAMEAREA_LEFT_TOP.y,
		GAMEAREA_LEFT_TOP.x + GAMEAREA_SIZE.x, GAMEAREA_LEFT_TOP.y + GAMEAREA_SIZE.y,
		GetColor( 255, 255, 255 ), FALSE
	);

	player.draw();
}

Utility::square< int > Game::get_game_area() const
{
	return { GAMEAREA_LEFT_TOP, GAMEAREA_SIZE };
}

unsigned Game::get_game_count() const
{
	return game_count;
}

VCtrlPtr Game::get_input() const
{
	return controller;
}

