#include <DxLib.h>
#include <array>
#include <algorithm>
#include <string>
#include "DxKeyboard.h"
#include "Keyboard.h"

bool DxKeyboard::proc()
{
	GetHitKeyStateAll( keybuff_ );
	return true;
}

/*
*	もうちょっとうまくできそう
*	もしかしたら仕様を変更するかも
*	とりあえず使いそうなキーを用意
*	思いつきで書いてる
*/

static constexpr std::array< int, Keyboard::KEY_MAX > dx_keymap =
{
	KEY_INPUT_A, KEY_INPUT_B, KEY_INPUT_C, KEY_INPUT_D, KEY_INPUT_E, KEY_INPUT_F, KEY_INPUT_G,
	KEY_INPUT_H, KEY_INPUT_I, KEY_INPUT_J, KEY_INPUT_K, KEY_INPUT_L, KEY_INPUT_M, KEY_INPUT_N,
	KEY_INPUT_O, KEY_INPUT_P, KEY_INPUT_Q, KEY_INPUT_R, KEY_INPUT_S, KEY_INPUT_T, KEY_INPUT_U,
	KEY_INPUT_V, KEY_INPUT_W, KEY_INPUT_X, KEY_INPUT_Y, KEY_INPUT_Z,
	KEY_INPUT_LSHIFT, KEY_INPUT_LCONTROL,
	KEY_INPUT_UP, KEY_INPUT_RIGHT, KEY_INPUT_DOWN, KEY_INPUT_LEFT
};

bool DxKeyboard::is_input( std::string const& keyname ) const
{
	const auto first = std::begin( Keyboard::KEY );
	const auto last = std::end( Keyboard::KEY );
	const auto it = std::find( first, last, keyname );

	if( it == last ) {
		return false;
	}
	
	auto d = std::distance( first, it );
	return keybuff_[ dx_keymap[ d ] ] != 0;
}

