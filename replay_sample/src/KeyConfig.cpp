#include <array>
#include <memory>
#include "Utility.h"
#include "VirtualController.h"
#include "KeyConfig.h"
#include "Debug.h"

static const std::string config_file_name = "key.config";

static const
std::array< std::string, KeyConfig::GAME_USE_KEY > default_key_name =
{
	"UP", "RIGHT", "DOWN", "LEFT"
};

static const
std::array< std::string, KeyConfig::GAME_USE_KEY > default_pad_button =
{
	"UP", "RIGHT", "DOWN", "LEFT",
};

KeyConfig::KeyConfig( SystemContext* system, std::shared_ptr< OnInputListener > listener ) :
	system( system ),
	listener( listener ),
	config_file( config_file_name ),
	config_pad(),
	config_key()
{
	// ないなら作る
	if( config_file.fail() ) {
		std::ofstream ofs( config_file_name );
		for( int i=0 ; i < GAME_USE_KEY ; ++i ) {
			ofs << default_key_name[i] << " " << default_pad_button[i] << " \n";
		}

		debug_console::print( "ファイルがなかったから作成したよ:", config_file_name );
	}
	else {
		for( int i=0 ; i<GAME_USE_KEY && !config_file.eof() ; ++i ) {
			std::string buf;
			std::getline( config_file, buf );
			auto&& splited = Utility::split( buf, ' ' );
			config_key[i] = std::move( splited[0] );
			config_pad[i] = std::move( splited[1] );
		}
	}
}

KeyConfig::~KeyConfig()
{
}

static constexpr
VirtualController::key_type key_map[ KeyConfig::GAME_USE_KEY ] =
{
	VirtualController::KEY_UP,
	VirtualController::KEY_RIGHT,
	VirtualController::KEY_DOWN ,
	VirtualController::KEY_LEFT,
};

VirtualController::key_type KeyConfig::get_state() const
{
	VirtualController::key_type input = 0;
	
	// キーボードとゲームパッドの入力を取得
	const auto keyboard = system->get_input_device()->get_keyboard();
	const auto joypad   = system->get_input_device()->get_joypad();

	for( int i=0 ; i < GAME_USE_KEY ; ++i ) {
		if(
			keyboard->is_input( config_key[i] ) ||
			joypad  ->is_input( config_pad[i] )
		) {
			input |= key_map[i];
		}
	}

	// これで入力状態を通知する
	if( input != 0 ) {
		listener->on_input( input );
	}
	
	return input;
}

