#pragma once
#include <memory>
#include "Keyboard.h"
#include "Joypad.h"
#include "GettableInputDevice.h"

class GettableDxInputDevice :
	public GettableInputDevice
{
	std::shared_ptr< Keyboard >		keyboard;
	std::shared_ptr< Joypad >		joypad;

public:

	GettableDxInputDevice();
	~GettableDxInputDevice() = default;

	std::shared_ptr< Keyboard > get_keyboard() const override;
	std::shared_ptr< Joypad > get_joypad() const override;
};
