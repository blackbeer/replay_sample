#include "Joypad.h"
#include <string>
#include <array>

const std::array< std::string, 4 > Joypad::BUTTON
{
	"UP", "RIGHT", "DOWN", "LEFT",
};

