#pragma once
#include "Debug.h"
#include "GameContext.h"
#include "VirtualController.h"
#include "Utility.h"
#include <vector>
#include <string>
#include <fstream>

/*
*	リプレイを読み込み、入力状態を返すコントローラー
*/
class ReplayPlayer :
	public VirtualController
{
	mutable unsigned	read_counter;
	GameContext*	game;
	std::ifstream	read_data;
	mutable std::vector< std::pair< unsigned, key_type > > replay_data;

public:

	ReplayPlayer( GameContext* game, std::string const& file );
	~ReplayPlayer() = default;

	VirtualController::key_type get_state() const override;

	// リプレイを最初から再生したいときに呼ぶ
	void from_the_beginning();

private:

	bool load_replay_file( std::string const& file );
};
