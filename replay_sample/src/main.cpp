#include <DxLib.h>
#include <memory>
#include "GettableInputDevice.h"
#include "GettableDxInputDevice.h"
#include "SystemContext.h"
#include "Game.h"

class App :
	public SystemContext
{
	Game	game;
	std::shared_ptr< GettableInputDevice > input;

public:

	App() :
		game( this ),
		input( std::make_shared< GettableDxInputDevice >() )

	{
		ChangeWindowMode( TRUE );
		DxLib_Init();
		SetDrawScreen( DX_SCREEN_BACK );
	}

	~App()
	{
		DxLib_End();
	}

	void run()
	{
		while( !ScreenFlip() && !ProcessMessage() && !ClearDrawScreen() ) {
			if( CheckHitKey( KEY_INPUT_ESCAPE ) != 0 ) break;
			input->get_keyboard()->proc();
			input->get_joypad()->proc();

			game.proc();
			game.draw();
		}
	}

	std::shared_ptr< GettableInputDevice > get_input_device() const override
	{
		return input;
	}
};

#include "Debug.h"

int WINAPI WinMain( HINSTANCE, HINSTANCE, LPSTR, int )
{
	debug_console dbc;
	App {}.run();
	return 0;
}

