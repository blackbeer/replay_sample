#pragma once

/*
*	仮想コントローラー
*/
class VirtualController
{
public:

	using key_type = int;

	static constexpr key_type KEY_UP    = 1;
	static constexpr key_type KEY_RIGHT = 2;
	static constexpr key_type KEY_DOWN  = 4;
	static constexpr key_type KEY_LEFT  = 8;

public:

	VirtualController() = default;
	virtual ~VirtualController() = default;

	virtual key_type get_state() const = 0;
};

#include <memory>
using VCtrlPtr = std::shared_ptr< VirtualController >;
