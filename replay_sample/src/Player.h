#pragma once

#include "GameContext.h"
#include "Utility.h"

class Player
{
	GameContext*	game;
	Utility::pos_t< int >	pos;

public:

	Player( GameContext* game );
	~Player();

	bool proc();
	void draw() const;
};

