#include "Keyboard.h"
#include <string>
#include <array>

const std::array< std::string, Keyboard::KEY_MAX > Keyboard::KEY =
{
	"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
	"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
	"LSHIFT", "LCTRL", "UP", "RIGHT", "DOWN", "LEFT"
};
