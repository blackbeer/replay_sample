#include "Debug.h"
#include "GameContext.h"
#include "VirtualController.h"
#include "Utility.h"
#include "ReplayPlayer.h"
#include <vector>
#include <utility>
#include <iterator>
#include <fstream>


ReplayPlayer::ReplayPlayer( GameContext* game, std::string const& file ) :
	read_counter(),
	game( game ),
	read_data()
{
	this->load_replay_file( file );
}

VirtualController::key_type ReplayPlayer::get_state() const
{
	const auto first = std::begin( replay_data );
	const auto last  = std::end( replay_data );
	const auto now   = game->get_game_count();
	const auto it = std::find_if(
		first + read_counter,
		last,
		[ now ]( auto&& e ) {
			return e.first == now;
		}
	);

	if( it == last ) return 0;
	++read_counter;
	return it->second;
}

void ReplayPlayer::from_the_beginning()
{
	read_counter = 0;
}

// private

bool ReplayPlayer::load_replay_file( std::string const& file )
{
	std::ifstream ifs( file );

	if( ifs.fail() ) {
		debug_console::print( "リプレイファイルないよ: ", file );
		return false;
	}
	else {
		std::string state {};
		while( true ) {
			std::getline( ifs, state );
			if( ifs.eof() ) break;
			auto&& splited = Utility::split( state, ' ' );
			replay_data.emplace_back(
				std::move( std::stoi( splited[ 0 ] ) ),
				std::move( std::stoi( splited[ 1 ] ) )
			);
		}
	}

	return true;
}
