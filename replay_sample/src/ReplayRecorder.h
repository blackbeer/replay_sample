#pragma once

#include "OnInputListener.h"
#include "VirtualController.h"
#include "GameContext.h"
#include <fstream>

class ReplayRecorder :
	public OnInputListener
{

	GameContext*	game;
	std::fstream	replay_file;
	std::vector< std::pair< unsigned, VirtualController::key_type > > record_data;

public:

	ReplayRecorder( GameContext* game );
	~ReplayRecorder();

	void on_input( VirtualController::key_type input ) override;

private:

	void output_file();
};

