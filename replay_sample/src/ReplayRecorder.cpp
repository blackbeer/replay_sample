#include "OnInputListener.h"
#include "VirtualController.h"
#include "GameContext.h"
#include "ReplayRecorder.h"
#include "Debug.h"
#include <fstream>

ReplayRecorder::ReplayRecorder( GameContext* game ) :
	game( game ),
	replay_file(),
	record_data()
{
	if( !Utility::is_exists( "replay.rpy" ) ) {
		Utility::make_new_file( "replay.rpy" );
		debug_console::print( "repファイルつくったよ" );
	}
}

ReplayRecorder::~ReplayRecorder()
{
	this->output_file();
}

void ReplayRecorder::on_input( VirtualController::key_type input )
{
	record_data.emplace_back( game->get_game_count(), input );
}

void ReplayRecorder::output_file()
{
	// とりあえず、無加工で出力する
	replay_file.open( "replay.rpy" );

	for( auto&& e : record_data ) {
		replay_file << e.first << " " << e.second << " \n";
	}

	replay_file.close();
}

