#include "GettableDxInputDevice.h"
#include "DxKeyboard.h"
#include "DxJoypad.h"
#include <memory>


GettableDxInputDevice::GettableDxInputDevice() :
	keyboard( std::make_shared< DxKeyboard >() ),
	joypad( std::make_shared< DxJoypad >() )
{
}

std::shared_ptr< Keyboard > GettableDxInputDevice::get_keyboard() const 
{
	return keyboard;
}

std::shared_ptr< Joypad > GettableDxInputDevice::get_joypad() const
{
	return joypad;
}

