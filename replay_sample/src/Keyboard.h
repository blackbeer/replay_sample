#pragma once

#include <string>
#include <array>

class Keyboard
{
public:

	// 使えるキー数
	static constexpr int KEY_MAX = 32;
	// 使えるキー名
	static const std::array< std::string, KEY_MAX > KEY;

public:

	Keyboard() = default;
	virtual ~Keyboard() = default;

	virtual bool proc() = 0;
	virtual bool is_input( std::string const& keyname ) const = 0;
};
