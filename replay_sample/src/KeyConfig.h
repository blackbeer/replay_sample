#pragma once

#include "SystemContext.h"
#include "VirtualController.h"
#include "OnInputListener.h"
#include <array>
#include <fstream>

class KeyConfig :
	public VirtualController
{
public:

	static constexpr int GAME_USE_KEY = 4;
private:

	SystemContext*		system;
	std::shared_ptr< OnInputListener >	listener;

	std::fstream			config_file;
	std::array< std::string, KeyConfig::GAME_USE_KEY >	config_pad;
	std::array< std::string, KeyConfig::GAME_USE_KEY >	config_key;

public:

	KeyConfig( SystemContext* system, std::shared_ptr< OnInputListener > listener );
	~KeyConfig() ;

	key_type get_state() const override;
};
