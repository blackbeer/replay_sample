#include "Player.h"
#include "VirtualController.h"
#include <DxLib.h>

Player::Player( GameContext* game ) :
	game( game ),
	pos()
{
	const auto area = game->get_game_area();

	pos.x = area.left_top.x + area.size.x / 2;
	pos.y = area.left_top.y + area.size.y * 3 / 4;
}

Player::~Player()
{
}

bool Player::proc()
{
	static const auto area = game->get_game_area();
	static const Utility::pos_t< int > right_down {
		area.left_top.x + area.size.x,
		area.left_top.y + area.size.y
	};
	const auto input = game->get_input()->get_state();

	if( input & VirtualController::KEY_UP ) { pos.y--; }
	if( input & VirtualController::KEY_DOWN ) { pos.y++; }
	if( input & VirtualController::KEY_RIGHT ) { pos.x++; }
	if( input & VirtualController::KEY_LEFT ) { pos.x--; }

	pos.x = Utility::clamp( pos.x, area.left_top.x, right_down.x );
	pos.y = Utility::clamp( pos.y, area.left_top.y, right_down.y );

	return true;
}

void Player::draw() const
{
	// TODO : ここのDxLib依存を辞める
	DrawCircle( pos.x, pos.y, 5, GetColor( 255, 0, 0 ) );
}
